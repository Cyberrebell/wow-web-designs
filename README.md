# wow-web-designs
Collection of different official WoW website designs and static content

Restored using wayback machine. Some style fixes to work for modern browsers.

# Content
* beta worldofwarcraft.com
* vanilla wow-europe.com

# How to run
`docker-compose up -d`

Open http://127.0.0.1/